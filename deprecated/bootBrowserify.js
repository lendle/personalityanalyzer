const EEGDataGetter=require('./EEGDataGetter.js');
const GSRDataGetter=require('./GSRDataGetter.js');
const FaceDataGetter=require('./FaceDataGetter.js');
const DataAggregator=require('./DataAggregator.js');

global.classes={
    EEGDataGetter:EEGDataGetter,
    GSRDataGetter:GSRDataGetter,
    FaceDataGetter:FaceDataGetter,
    DataAggregator
};