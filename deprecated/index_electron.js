const { app, BrowserWindow } = require('electron');

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let win=null;

function createWindow () {
  win = new BrowserWindow({ width: 1024, height: 700 });
  win.loadFile('psychological_tool.html');
  win.on('closed', () => {
    win = null;
  });
}

app.on('ready', createWindow);
