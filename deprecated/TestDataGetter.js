const DataGetter=require('./DataGetter.js');
class TestDataGetter extends DataGetter{
    constructor(name, applicationId, clientId, interval){
        super(name, applicationId, clientId, interval);
        this.sample={"applicationId":applicationId,"content":JSON.parse("{\"data\":651}"),"readBy":["client1"],"timestamp":-1,"ttlMS":30000};
    }

    transform(data){
        return data;
    }

    getFields(){
        return ["data"];
    }
}

let testDataGetter=new TestDataGetter('test', 'test', 'client1', 500);
testDataGetter.on('data', (data)=>{
	console.log(data);
});
testDataGetter.start();

module.exports=TestDataGetter;

