const EEGDataGetter=require('./EEGDataGetter.js');
const GSRDataGetter=require('./GSRDataGetter.js');
const FaceDataGetter=require('./FaceDataGetter.js');
const DataAggregator=require('./DataAggregator.js');

let eEGDataGetter=new EEGDataGetter("", "eeg-192.168.0.7", "client1", 500);
let gSRDataGetter=new GSRDataGetter('', 'gsr-192.168.0.13', 'client1', 500);
let faceDataGetter=new FaceDataGetter('', 'face-id127.1.1.1', 'client1', 500);
//setInterval(function(){}, 500);

let dataAggregator=new DataAggregator([eEGDataGetter, gSRDataGetter, faceDataGetter], 3000);
dataAggregator.on('data', data=>{
    console.log(data);
});

eEGDataGetter.start(true);
gSRDataGetter.start(true);
faceDataGetter.start(true);
