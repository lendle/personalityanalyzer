import { AbstractDataGetter } from './AbstractDataGetter';

class SampleDirectDataGetter extends AbstractDataGetter {
    constructor(name="sample", interval=1000, outputInterval=-1, aggregationFunction=AbstractDataGetter.mean) {
        super(name, interval, outputInterval, aggregationFunction);
    }
    processARound(callback) {
        // let data = {
        //     "applicationId": "test",
        //     "content": { "time": new Date().getTime(), "data": Math.random() * 100 },
        //     "readBy": ["sample_client"], "timestamp": new Date().getTime(), "ttlMS": 20000
        // };
        let data={
            timestamp: Date.now(),
            value: Math.random() * 100
        };
        callback(data);
    }
}

export { SampleDirectDataGetter };