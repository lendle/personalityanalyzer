import { AbstractDataGetter } from './AbstractDataGetter'
const mathjs=require('mathjs');

class MetaDataGetter extends AbstractDataGetter {
    static mean(list) {
        return mathjs.mean(list);
    }
    static max(list) {
        return mathjs.max(list);
    }
    static min(list) {
        return mathjs.min(list);
    }
    static median(list) {
        return mathjs.median(list);
    }
    constructor(name, subDataGetters, interval, aggregationFunction = MetaDataGetter.mean) {
        super(name, interval);
        this.aggregationFunction = aggregationFunction;
        this.subDataGetters = subDataGetters;
        this.subDataGetterDataBuffers = {};
        let self = this;
        this.lastErrors = [];
        for (let g of this.subDataGetters) {
            this.subDataGetterDataBuffers[g.getName()] = [];
            g.on('data', (data) => {
                self.subDataGetterDataBuffers[g.getName()].push(data);
            });
            g.on('error', (error) => {
                console.log(error);
                this.lastErrors.push(error);
            });
        }
    }

    start(){
        for (let g of this.subDataGetters) {
            g.start();
        }
        super.start();
    }

    stop(){
        for (let g of this.subDataGetters) {
            g.stop();
        }
        super.stop();
    }
    /**
     * return [largest timestamp, data]
     * @param {*} dataList 
     */
    aggregate(dataList) {
        dataList.sort((a, b) => {
            a.timestamp - b.timestamp;
        });
        let buffer = [];
        for (let data of dataList) {
            buffer.push(data.value);
        }
        return [dataList[dataList.length-1].timestamp, this.aggregationFunction(buffer)];
    }

    processARound(callback) {
        if (this.lastErrors.length > 0) {
            //there are errors in at least one DataGetter
            this.eventEmitter.emit('error', [...this.lastErrors]);
            this.lastErrors.length = 0;
        } else {
            let ready = true;
            for (let g of this.subDataGetters) {
                if (this.subDataGetterDataBuffers[g.getName()].length == 0) {
                    ready = false;
                    break;
                }
            }
            if (ready) {
                let data = {
                    timestamp: Date.now()
                };
                for (let g of this.subDataGetters) {
                    let aggregatedValue = this.aggregate(this.subDataGetterDataBuffers[g.getName()]);
                    data[g.getName()] = aggregatedValue[1];
                    data.timestamp=aggregatedValue[0];
                    this.subDataGetterDataBuffers[g.getName()].length = 0;
                }
                callback(data);
            }
        }
    }
}

export {MetaDataGetter};