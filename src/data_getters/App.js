import {SampleDirectDataGetter} from './SampleDirectDataGetter';
import {AppMessageDataGetter} from './AppMessageDataGetter';
import {AbstractDataGetter} from './AbstractDataGetter';
import {MetaDataGetter} from './MetaDataGetter';

// let getter=new SampleDirectDataGetter(1000, 10000);
// getter.on('data', (d)=>{
//     console.log("from SampleDirectDataGetter: "+JSON.stringify(d));
// });
// //getter.start();


let getter1=new AppMessageDataGetter("app", "gsr", "sample_client", 500, 3000, AbstractDataGetter.max);
getter1.on('data', (d)=>{
    //console.log("from AppMessageDataGetter: "+JSON.stringify(d));
});
//getter1.start();

let metaGetter=new MetaDataGetter("meta", [/*getter*/getter1], 3000);
metaGetter.on('data', (d)=>{
    console.log("from MetaDataGetter: "+JSON.stringify(d));
});
metaGetter.start();