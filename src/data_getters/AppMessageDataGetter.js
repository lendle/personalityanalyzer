import { AbstractDataGetter } from './AbstractDataGetter';
const request = require('request-promise');
/**
 * get data from http://imsofa.rocks:8080/LabUtils/ws/app-messages/
 */
class AppMessageDataGetter extends AbstractDataGetter {
    /**
     * 
     * @param {*} name the name of this instance
     * @param {*} applicationId the name of application under which the data is sent
     * @param {*} clientId the id of the client
     * @param {*} interval the interval to check new data
     * @param {*} outputInterval the interval to output data, should be larger than interval
     * @param {*} aggregationFunction the aggregation function to use
     */
    constructor(name, applicationId, clientId, interval) {
        super(name, interval);
        this.applicationId=applicationId;
        this.clientId=clientId;
        this.url = "http://imsofa.rocks:8080/LabUtils/ws/app-messages/" + applicationId + "/" + clientId;
    }

    processARound(callback) {
        let p = request(this.url);
        p.then(data => {
            if(data && data.length>1){
                if(typeof(data)=="string"){
                    data=JSON.parse(data);
                }
                if(typeof(data.content)=="string"){
                    data.content=JSON.parse(data.content);
                }
            }else{
                data=null;
            }
            if(data){
                callback({timestamp: data.content.time, value: data.content.data});
            }else{
                callback(null);
            }
            
        }).catch(err => {
            callback(null, err);
        });
    }
}

export { AppMessageDataGetter }