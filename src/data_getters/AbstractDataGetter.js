const EventEmitter = require('events');
/**
 * an abstract interface to be implemented by
 * sub DataGetters
 * sub classes should implement the processARound function
 */

class AbstractDataGetter{
    /**
     * 
     * @param {*} name the name of this instance
     * @param {*} interval the interval to check new data
     */
    constructor(name, interval) {
        this.name=name;
        this.eventEmitter = new EventEmitter();
        this.interval = interval;
        this.running = false;
    }

    getName(){
        return this.name;
    }

    /**
     * start the listening process
     */
    start() {
        this.running = true;
        setTimeout(() => { this.loop(); }, this.interval);
    }
    /**
     * stop the listening process
     */
    stop() {
        this.running = false;
    }
    /**
     * override to transform data
     * @param {*} data 
     */
    transform(data) {
        return data;
    }

    loop(){
        if(!this.running){
            setTimeout(() => { this.loop(); }, this.interval);
        }else{
            let self=this;
            this.processARound(function(data, error){
                if(!error){
                    if (data && (""+data).length > 1) {
                        data.value=self.transform(data.value);
                        self.eventEmitter.emit('data', data);
                    }
                }else{
                    self.eventEmitter.emit('error', error);
                }
                setTimeout(() => { self.loop(); }, self.interval);
            });
        }
    }

    /**
     * sub classes should implement this function
     * and pass retrieved data to the main loop via
     * the callback parameter
     * the data must be in the form of
     * {
     *  timestamp: xxx,
     *  value: xxx
     * }
     * @param {*} callback function(data, error)
     */
    processARound(callback) {
        
    }

    getFields() {
        return [];
    }

    on(event, callback) {
        this.eventEmitter.on(event, callback);
    }
}

export {AbstractDataGetter};