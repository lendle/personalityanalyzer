/**
 * This file will automatically be loaded by webpack and run in the "renderer" context.
 * To learn more about the differences between the "main" and the "renderer" context in
 * Electron, visit:
 *
 * https://electronjs.org/docs/tutorial/application-architecture#main-and-renderer-processes
 *
 * By default, Node.js integration in this file is disabled. When enabling Node.js integration
 * in a renderer process, please be aware of potential security implications. You can read
 * more about security risks here:
 *
 * https://electronjs.org/docs/tutorial/security
 *
 * To enable Node.js integration in this file, open up `main.js` and enable the `nodeIntegration`
 * flag:
 *
 * ```
 *  // Create the browser window.
 *  mainWindow = new BrowserWindow({
 *    width: 800,
 *    height: 600,
 *    webPreferences: {
 *      nodeIntegration: true
 *    }
 *  });
 * ```
 */

import './index.css';
import { AppMessageDataGetter } from './data_getters/AppMessageDataGetter';
import { MetaDataGetter } from './data_getters/MetaDataGetter';
import { SampleDirectDataGetter } from './data_getters/SampleDirectDataGetter';
import 'jquery';
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
const { dialog } = require('electron').remote;
const { saveCSV, loadCSV } = require('electron').remote.require('../main');

var getter1 = new SampleDirectDataGetter("gsr", 500, 500);
// var getter1 = new AppMessageDataGetter("gsr", "gsr", "sample_client", 500);
var metaGetter = new MetaDataGetter("meta", [getter1], 1000);

metaGetter.on('data', (d) => {
    //console.log("from MetaDataGetter: " + JSON.stringify(d));
    addData(d);
});

function startButtonClicked() {
    $("#btnStart").attr('disabled', 'disabled');
    $("#btnLoad").attr('disabled', 'disabled');
    $("#btnStop").removeAttr('disabled');
    $("#player").get(0).play();
    metaGetter.start();
}

function stopButtonClicked() {
    $("#btnStop").attr('disabled', 'disabled');
    $("#btnCSV").removeAttr('disabled');
    $("#btnLoad").removeAttr('disabled');
    metaGetter.stop();
    $("#player").get(0).pause();
}
/**
 * the original implementation of google.visualization.dataTableToCsv
 * will convert timestamp to string, so I have to implement my own
 * @param {*} data 
 */
function dataTableToCsv(data) {
    let result = [];
    for (let i = 0; i < data.getNumberOfRows(); i++) {
        let row = [];
        for (let j = 0; j < data.getNumberOfColumns(); j++) {
            row.push(data.getValue(i, j));
        }
        result.push(row.join(","));
    }
    return result.join("\n");
}

function csvButtonClicked() {
    //let csv = google.visualization.dataTableToCsv(data);
    let csv = dataTableToCsv(data);
    csv = "timestamp,gsr\n" + csv;
    //console.log(csv);
    let filepath = dialog.showSaveDialogSync(null, {
        filters: [
            {
                name: "CSV",
                extensions: ['csv']
            }
        ],
        defaultPath: ""+token+".csv"
    });
    if (filepath) {
        saveCSV(filepath, csv);
    }
}

function loadButtonClicked() {
    $("#dataLoadingIndicator").show();
    let filepath = dialog.showOpenDialogSync(null, {
        filters: [
            {
                name: "CSV",
                extensions: ['csv']
            }
        ]
    });
    let records = loadCSV(filepath[0]);
    let firstLine = true;
    for (let record of records) {
        if (firstLine) {
            firstLine = false;
            continue;
        }
        //console.log(record);
        addData({ timestamp: record[0], gsr: record[1] });
    }
    setTimeout(function () {
        refreshChart();
        $("#dataLoadingIndicator").hide();
    }, 5000);

}

window.$ = $;
$(document).ready(function () {
    token=Math.random().toString(36).substr(2,5)+Date.now();
    $("#token").text(token);
    $("#googleForm").attr("src", "https://docs.google.com/forms/d/e/1FAIpQLSftzvJwV19eUf910SWaZtlIQveyj5LDPpQnGu_BLkHdGVF4EA/viewform?usp=pp_url&entry.791390980="+token);
    $("#btnStart").click(startButtonClicked);
    $("#btnStop").click(stopButtonClicked);
    $("#btnCSV").click(csvButtonClicked);
    $("#btnLoad").click(loadButtonClicked);
    google.charts.load('current', { 'packages': ['line', 'controls'] });
    google.charts.setOnLoadCallback(drawChart);
    $("#loading").hide();
    $("#container").show();
    $("#btnReload").click(function(){
        $("#googleForm").attr("src", "about:blank");
        location.reload();
    });

    // $(".tab-pane").removeClass("active");
    // $("#questionnaire").addClass("active");
    // $(".nav-item a[href='#questionnaire']").tab("show");
});